package com.muradnajafli.messagingapp

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.muradnajafli.messagingapp.databinding.MessageItemBinding

class MessageAdapter : RecyclerView.Adapter<MessageAdapter.MessageViewHolder>() {
    private var messageList: List<Message> = emptyList()

    class MessageViewHolder(private val binding: MessageItemBinding) : ViewHolder(binding.root) {
        fun bind(messageItem: Message) {
            binding.apply {
                icon.setImageResource(R.drawable.notification_icon)
                titleData.text = messageItem.title
                messageData.text = messageItem.message
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val binding = MessageItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MessageViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        val message = messageList[position]
        holder.bind(message)
    }

    override fun getItemCount() = messageList.size

    @SuppressLint("NotifyDataSetChanged")
    fun setMessageList(newMessageList: List<Message>) {
        messageList = newMessageList
        notifyDataSetChanged()
    }
}