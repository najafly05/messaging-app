package com.muradnajafli.messagingapp

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import com.muradnajafli.messagingapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var messageAdapter: MessageAdapter
    private var myFirebaseMessagingService = MyFirebaseMessagingService()
    private val requestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
            if (isGranted) {
                showNotification()
            }
        }

    private fun showNotification() {
            myFirebaseMessagingService.title?.let { title ->
                myFirebaseMessagingService.messageBody?.let { message ->
                    myFirebaseMessagingService.generateNotification(Message(title, message))
                }
            }
    }

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRecyclerView()

        requestPermissionLauncher.launch(android.Manifest.permission.POST_NOTIFICATIONS)

    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onResume() {
        super.onResume()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            registerReceiver(updateMessageListReceiver,
                IntentFilter("UPDATE_MESSAGE_LIST"),
                RECEIVER_NOT_EXPORTED)
        }
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(updateMessageListReceiver)
    }

    private fun setupRecyclerView() {
        messageAdapter = MessageAdapter()
        binding.messageRv.adapter = messageAdapter
        binding.messageRv.layoutManager = LinearLayoutManager(this)
        updateMessageList()
    }

    private fun updateMessageList() {
        val messageList = myFirebaseMessagingService.getMessages()
        messageAdapter.setMessageList(messageList)
    }

    private val updateMessageListReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            updateMessageList()
        }
    }
}

