package com.muradnajafli.messagingapp

data class Message(
    val title: String,
    val message: String
)
